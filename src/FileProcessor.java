import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileProcessor {

    public void printContents(String fileName) throws IOException {

        String path = ConfigurationSettingsSingleton.INSTANCE.getPath();
        File file = new File(path + "\\" + fileName);

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr);){
            String line;
            while ((line = br.readLine()) != null){
                System.out.println(line);
            }
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

    }

}
