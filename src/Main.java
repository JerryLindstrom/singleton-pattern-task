import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        ConfigurationSettingsSingleton.INSTANCE.setPath("C:\\Users\\sssyds\\IdeaProjects\\Singleton-Pattern-Task");

        FileProcessor fileProcessor = new FileProcessor();
        try{
            fileProcessor.printContents("test.txt");
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
}
